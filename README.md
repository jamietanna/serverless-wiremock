# Serverless Wiremock, or "how to run wiremock without an HTTP server"

A sample project to show how to run Wiremock without its HTTP server executing requests, as found documented [on my blog](https://www.jvt.me/posts/2021/04/29/wiremock-serverless/).

This can be seen by running:

```sh
./gradlew clean run
```

Which should output the results of two Wiremock stubs.
