/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package me.jvt.hacking.wiremock;

import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.core.WireMockApp;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.http.*;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.google.common.base.Optional;
import org.eclipse.jetty.server.RequestLog;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class App {
    public static void main(String[] args) {
        Options options = WireMockConfiguration.options().usingFilesUnderClasspath("wiremock");
        WireMockApp app = new WireMockApp(options, null);
        StubRequestHandler handler = app.buildStubRequestHandler();

        RequestWrapper request0 = new RequestWrapper("/url", RequestMethod.GET, new HttpHeaders());
        ServeEvent event0 = handler.handleRequest(request0);
        ResponseDefinition response0  = event0.getResponseDefinition();
        System.out.println(response0);

        HttpHeader header1 = new HttpHeader("authorization", "Bearer foo");
        RequestWrapper request1 = new RequestWrapper("/url", RequestMethod.GET, new HttpHeaders(header1));
        ServeEvent event1 = handler.handleRequest(request1);
        ResponseDefinition response1  = event1.getResponseDefinition();
        System.out.println(response1);
    }

    public static class RequestWrapper implements Request {

        private final String url;
        private final RequestMethod method;
        private final HttpHeaders headers;

        public RequestWrapper(String url, RequestMethod method, HttpHeaders headers) {
            this.url = url;
            this.method = method;
            this.headers = headers;
        }

        @Override
        public String getUrl() {
            return url;
        }

        @Override
        public String getAbsoluteUrl() {
            return "http://localhost:8080/" + url;
        }

        @Override
        public RequestMethod getMethod() {
            return method;
        }

        @Override
        public String getScheme() {
            return "http";
        }

        @Override
        public String getHost() {
            return "localhost";
        }

        @Override
        public int getPort() {
            return 8080;
        }

        @Override
        public String getClientIp() {
            return null;
        }

        @Override
        public String getHeader(String key) {
            return headers.getHeader(key).firstValue();
        }

        @Override
        public HttpHeader header(String key) {
            return headers.getHeader(key);
        }

        @Override
        public ContentTypeHeader contentTypeHeader() {
            return headers.getContentTypeHeader();
        }

        @Override
        public HttpHeaders getHeaders() {
            return headers;
        }

        @Override
        public boolean containsHeader(String key) {
            return headers.getHeader(key).isPresent();
        }

        @Override
        public Set<String> getAllHeaderKeys() {
            return headers.keys();
        }

        @Override
        public Map<String, Cookie> getCookies() {
            return Collections.emptyMap(); // TODO: would be usable
        }

        @Override
        public QueryParameter queryParameter(String key) {
            return null; // TODO: would be parsed
        }

        @Override
        public byte[] getBody() {
            return new byte[0]; // TODO: would be usable
        }

        @Override
        public String getBodyAsString() {
            return null; // TODO: would be usable
        }

        @Override
        public String getBodyAsBase64() {
            return null; // TODO: would be usable
        }

        @Override
        public boolean isMultipart() {
            return false; // TODO: would be usable
        }

        @Override
        public Collection<Part> getParts() {
            return Collections.emptySet(); // TODO: would be usable
        }

        @Override
        public Part getPart(String name) {
            return null; // TODO: would be usable
        }

        @Override
        public boolean isBrowserProxyRequest() {
            return false; // TODO: would be usable
        }

        @Override
        public Optional<Request> getOriginalRequest() {
            return Optional.absent();
        }
    }
}
